//add event listener to textbox
const input = document.querySelector('input');
input.addEventListener('input', calculate);

//add event listener to drop-down
const calc = document.getElementById('calculation');
calc.addEventListener('change', calculate);

function calculate() {
    var inputValue = document.getElementById('input').value;
    const log = document.getElementById('log');
    var answer;

    if (inputValue != ""){
        var value = checkValue(inputValue);
        var calcType = document.getElementById("calculation").value;
        if (calcType == 'isPrime'){
            answer = isPrime(value);
        }
        else if (calcType == 'isFibonacci'){
            answer = isFibonacci(value);
        }
    }
    else {
        answer = "";
    }
    log.textContent = answer;
}

function checkValue(num) {
    var correctValue;

    //if input is negative, replace with 1
    if (num < 0){
        correctValue = 1
    }
    //else if input is not integer, round to the nearest integer
    else if (isFloat(num)){
        correctValue = Math.round(num);
    }
    //else input is a positive integer, use the input value
    else {
        correctValue = num;
    }

    //replace original value in the input text box and return it
    document.getElementById("input").value = correctValue;
    return correctValue;
    
}

function isFloat(num){
    return Number(num) == num && num % 1 !== 0;
}

function isPrime(num) {
    for(var i = 2; i < num; i++)
        if(num % i === 0) return false;
    return num > 1;
}

function isFibonacci (num) {
    if (isSquare(5*(num*num)-4) || isSquare(5*(num*num)+4)) {
       return true;
    } else { return false; }
}

function isSquare(num) {
    return num > 0 && Math.sqrt(num) % 1 === 0;
};
